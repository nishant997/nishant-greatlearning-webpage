const form = document.getElementById('form')
const name = document.getElementById('name')
const email = document.getElementById('email')
const mobNumber = document.getElementById('mobNumber')
const experience = document.getElementById('experience')
const organization = document.getElementById('organization')

form.addEventListener('submit', (event) => {
	event.preventDefault()
	if ($('#form').valid()) {
		localStorage.setItem('name', name.value)
		localStorage.setItem('email', email.value)
		localStorage.setItem(' mobNumber', mobNumber.value)
		localStorage.setItem('experience', experience.value)
		localStorage.setItem('organization', organization.value)
	}
})

let acc = document.getElementsByClassName('accordion')

for (let i = 0; i < acc.length; i++) {
	acc[i].addEventListener('click', function () {
		acc[i].classList.toggle('active')
		let panel = acc[i].nextElementSibling
		for (let j = 0; j < acc.length; j++) {
			if (acc[i] != acc[j]) {
				acc[j].classList.remove('active')
				acc[j].nextElementSibling.style.maxHeight = null
			}
		}
		if (panel.style.maxHeight) {
			panel.style.maxHeight = null
		} else {
			panel.style.maxHeight = panel.scrollHeight + 'px'
		}
	})
}

var mybutton = document.getElementById('myBtn')
window.onscroll = function () {
	scrollFunction()
}

function scrollFunction() {
	if (
		document.body.scrollTop > 540 ||
		document.documentElement.scrollTop > 540
	) {
		mybutton.style.display = 'flex'
	} else {
		mybutton.style.display = 'none'
	}
}
$(document).ready(function () {
	$('a').on('click', function (event) {
		if (this.hash !== '') {
			event.preventDefault()
			var hash = this.hash

			$('html, body').animate(
				{
					scrollTop: $(hash).offset().top,
				},
				800,
				function () {
					window.location.hash = hash
				}
			)
		}
	})
})

let slideIndex = 1
showSlides(slideIndex)

function plusSlides(n) {
	showSlides((slideIndex += n))
}

function currentSlide(n) {
	showSlides((slideIndex = n))
}

function showSlides(n) {
	let i
	let slides = document.getElementsByClassName('mySlides')
	let dots = document.getElementsByClassName('dot')
	if (n > slides.length) {
		slideIndex = 1
	}
	if (n < 1) {
		slideIndex = slides.length
	}
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = 'none'
	}
	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(' working', '')
	}
	slides[slideIndex - 1].style.display = 'block'
	dots[slideIndex - 1].className += ' working'
}

$(function () {
	var $registerForm = $('#form')
	if ($registerForm.length) {
		$registerForm.validate({
			rules: {
				name: 'required',
				email: {
					required: true,
					email: true,
				},
				mobNumber: {
					required: true,
					minlength: 10,
				},
				experience: 'required',
				organization: {
					required: true,
				},
				checkbox: 'required',
			},
			messages: {
				name: 'Please enter user name',
				email: {
					required: 'Please enter email address',
					email: 'Please enter a valid email address',
				},
				mobNumber: {
					required: 'Please provide a phone number',
					minlength: 'please provide valid phone number',
				},
				experience: 'please provide your experience',
				organization: {
					required: 'Please select organization!',
				},
				checkbox: 'Please authorize',
			},
			errorPlacement: function (error, element) {
				if (element.is(':checkbox')) {
					error.insertAfter(element.parents('.checkbox_input'))
				} else {
					error.insertAfter(element)
				}
			},
		})
	}
})
